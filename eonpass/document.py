# MIT License - Eonpass 2019

# Methods for creating documents
import hashlib
import binascii

def getBody(payload):
    currentSealTxid = payload['currentSealTxid']
    currentSealVout = payload['currentSealVout']
    nextSealTxid = payload['nextSealTxid']
    nextSealVout = payload['nextSealVout']
    notes = payload['notes']

    try:
        message = "seal: regtestelements, "+currentSealTxid+", "+str(currentSealVout)+"; "
        message += "next seal: regtestelements, "+currentSealTxid+", "+str(currentSealVout)+"; "
        message += "document_notes: "+notes
        message = hashlib.sha256(message.encode()).hexdigest()
        
        # prepare the message, check lenght
        msgHex = binascii.hexlify(message.encode('utf-8')).decode('utf-8')
        lengthMsg = int(len(msgHex)/2)

        # OP_RETURN 80 bytes max
        if lengthMsg > 80:
            print("Unfeasible OP_RETURN: ", lengthMsg, "bytes\n")
            return("Unfeasible OP_RETURN")

        return msgHex
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500
