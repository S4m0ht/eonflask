# MIT License - Eonpass 2019

# Methods for Seals

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import eonpass.config as config


def breakSeal(payload):

    txid = payload['txid']
    vout = payload['vout']
    msgHex = payload['msgHex']

    try:
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))

        ins = [
            {
                'txid': txid, 
                'vout': vout
            }
        ]

        outs = {
            "data": msgHex
        }

        rawTx = rpc_connection.createrawtransaction(ins, outs)
        fundedTx = rpc_connection.fundrawtransaction(rawTx) #fix fees
        blindedTx = rpc_connection.blindrawtransaction(fundedTx["hex"])
        signedTx = rpc_connection.signrawtransactionwithwallet(blindedTx)

        sentTx = rpc_connection.sendrawtransaction(signedTx['hex'])
        return (sentTx)

    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        return "Internal Error", 500
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500