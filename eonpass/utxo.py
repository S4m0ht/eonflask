# MIT License - Eonpass 2019

# Methods for UTXO

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import eonpass.config as config


def get():

    try:
        rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(config.RPC_USER, config.RPC_PASSWORD, config.RPC_HOST, config.RPC_PORT))

        utxos = rpc_connection.listunspent(1)

        i = 0
        v = -1
        txid = ''
        found = False
        for UTXO in utxos:
            rawTx=rpc_connection.getrawtransaction(UTXO['txid'], 1)
            #unblind to see if you have enought value
            unblindedTx = rpc_connection.unblindrawtransaction(rawTx['hex'])
            decodedUnblindedTx = rpc_connection.decoderawtransaction(unblindedTx['hex'])
            v = -1
            for vout in decodedUnblindedTx['vout']:
                print(vout)
                v+=1
                if (vout['scriptPubKey']['type']=='scripthash') and (vout.get('value')>=0.0001): #greater than some max fee
                    found=True
                    break
            if found:
                txid = rawTx['txid'] #use the blinded txid to communicate to the network
                break
            i+=1

        return {'txid':txid, 'vout': v}

    except JSONRPCException as json_exception:
        print("A JSON RPC Exception occured: " + str(json_exception))
        return "Internal Error", 500
    except Exception as general_exception:
        print("An Exception occured: " + str(general_exception))
        return "Internal Error", 500

    