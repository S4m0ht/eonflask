# EonFlask

Flask server to create single use seals over liquid.
Submission for Liquid Hack

```
pip3 install Flask
pip3 install python-bitcoinrpc
python3 eonflask.py
```

In the config file there are connection to a server set up for the hackaton, you can use to test it out.

MIT License - Eonpass 2019 (https://eonpass.com)