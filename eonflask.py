from __future__ import print_function
from flask import Flask, jsonify, request

import eonpass.utxo as Utxo
import eonpass.document as Document
import eonpass.seal as Seal

app = Flask(__name__)

@app.route('/')
def index():
    return 'welcome to Eonpass server for liquid hack demonstration!'

#GET an UTXO from the connected wallet
@app.route('/utxo', methods=['GET'])
def utxo():
    if request.method == 'GET':
        return jsonify(Utxo.get())

#POST a message and receive the hex of its hash to be used as OP_RETURN
@app.route('/message', methods=['POST'])
def message():
    if request.method == 'POST':
        payload = request.json
        return jsonify(Document.getBody(payload))

#POST a seal to be broken, which is a txid, the vout to spend, the message to put in the OP_RETURN
#this endpoint returns the new txid that is spent
@app.route('/brokenseal', methods=['POST'])
def brokenseal():
    if request.method == 'POST':
        payload = request.json
        return jsonify(Seal.breakSeal(payload))


        

app.run(debug=True)